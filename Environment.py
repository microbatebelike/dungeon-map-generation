import math
import re

# This class provide us with all the informations about the game :
# 1 - Check the available actions.
# 2 - Update the available actions.
# 3 - Update the state of the game based on a chosen action.
# 4 - Determine if a state is final or not.
# 5 - Determine the reward for a given action in a given state.


class Environment:

    def __init__(self, size, place_point=3, distance_reward=2, create_connection=10, break_wall=-4):
        self.size = size
        self.place_point = place_point
        self.distance_reward = distance_reward
        self.create_connection = create_connection
        self.break_wall = break_wall
        self.available_actions = []

    # Return the list of the available actions in a given state
    def get_list_of_actions(self):
        return self.available_actions

    # Update the list of available actions :
    # - When placing a point in a box we have to make sure that we remove any action
    #  that place another point in the same box, we have also to make sure that we removed
    # the given action for any other position
    # - When breking a wall we just have to remove the action from the given list
    def update_available_actions(self, chosen_action):
        numerical_order = r'[^a-zA-Z ]'
        alphabetical_order = r'[^0-9]'
        base_action = re.sub(numerical_order, '', chosen_action)
        number = re.sub(alphabetical_order, '', chosen_action)

        if 'Place' in base_action:
            L = list(self.get_list_of_actions())
            for action in L:

                if base_action in action:
                    self.available_actions.remove(action)
                elif ('Place' in action) and (number in action):
                    self.available_actions.remove(action)

        else:
            self.available_actions.remove(chosen_action)

    # - Initialize the list of actions with all the possible actions
    # - Initialize the state of the maze :
    #       - All the boxes are empty
    #       - All the walls are closed
    # - Returns this initial state
    def initialize(self):
        self.available_actions = ['Place the starting point ' + str(i+1) for i in range(self.size**2)] + ['Place the exit point ' + str(i+1) for i in range(
            self.size**2)] + ['Place the tresor ' + str(i+1) for i in range(self.size**2)] + ['Break the wall ' + str(i+1) for i in range((self.size-1)*self.size*2)]
        boxes = [0 for _ in range(self.size**2)]
        walls = [1 for _ in range((self.size-1)*self.size*2)]
        return boxes, walls

    # Update the state of the maze given a chosen action
    def update_state(self, state, chosen_action):
        boxes, walls = state
        numerical_order = r'[^a-zA-Z ]'
        alphabetical_order = r'[^0-9]'
        base_action = re.sub(numerical_order, '', chosen_action)
        number = re.sub(alphabetical_order, '', chosen_action)
        if 'Place' in base_action:
            if 'starting' in base_action:
                boxes[int(number)-1] = 1
            elif 'exit' in base_action:
                boxes[int(number)-1] = 2
            elif 'tresor' in base_action:
                boxes[int(number)-1] = 3

        elif 'wall' in base_action:
            walls[int(number)-1] = 0

        return boxes, walls

    # For any given wall it returns the indexes of the surrounding boxes :
    # - When the wall is vertical it returns the right and left boxes
    # - When the wall is horizontal it returns the upper and lower boxes

    def get_surrounding_boxes(self, wall_index):
        wall_number = wall_index+1

        if ((wall_number - 1) % ((self.size * 2) - 1)) >= (self.size - 1):
            upper_box_index = wall_number - \
                (math.ceil(wall_number / (self.size * 2 - 1)) * (self.size - 1))
            lower_box_index = upper_box_index + self.size
            return [int(math.ceil(upper_box_index))-1, int(math.ceil(lower_box_index))-1]
        else:
            right_box_index = wall_number - \
                (int(wall_number / (self.size * 2 - 1)) * (self.size - 1))
            left_box_index = right_box_index + 1

            return [int(math.ceil(right_box_index))-1, int(math.ceil(left_box_index))-1]

    # For any given box it returns the indexes of the surrounding walls
    def get_surrounding_walls(self, box_index):
        box_row_index = int((box_index) / self.size)
        box_col_index = int((box_index) % self.size)

        if box_row_index > 0:
            upper_wall_index = box_col_index + \
                (box_row_index-1)*self.size+(box_row_index)*(self.size-1)
            right_wall_index = upper_wall_index + self.size
            left_wall_index = upper_wall_index + (self.size - 1)
            lower_wall_index = upper_wall_index + ((self.size*2) - 1)

        if box_row_index == 0:
            upper_wall_index = -1
            right_wall_index = box_col_index
            left_wall_index = right_wall_index - 1
            lower_wall_index = box_col_index + self.size - 1

        if box_row_index == self.size - 1:
            lower_wall_index = -1

        if box_col_index == 0:
            left_wall_index = -1

        if box_col_index == self.size - 1:
            right_wall_index = -1

        walls = list((left_wall_index, upper_wall_index,
                     right_wall_index, lower_wall_index))
        return [i for i in walls if i != -1]

    # It checks if there is a connections between two given boxes
    # by finding all the adjacent boxes of the first box
    # then checking if the second box is in those adjacent boxes
    def check_connection(self, state, first_index, second_index):
        _, walls = state
        possibilities = [first_index]
        connections = []

        while len(possibilities) > 0:
            possibility = possibilities.pop()
            connections.append(possibility)
            surrounding_walls = self.get_surrounding_walls(possibility)

            for wall_index in surrounding_walls:
                if walls[wall_index] == 0:
                    possibilities.extend(
                        self.get_surrounding_boxes(wall_index))
            possibilities = [
                box for box in possibilities if box not in connections and box >= 0 and box < self.size * self.size]

        if second_index in connections:
            return True
        else:
            return False

    # It checks if a given state is a final state by checking:
    # - if all the tree points are placed
    # - There is a connection between the starting and ending point
    # - There is a connection between the starting and tresor point
    def is_final_state(self, state):
        boxes, _ = state
        for i in [1, 2, 3]:
            if i not in boxes:
                return False
        return self.check_connection(state, boxes.index(1), boxes.index(2)) and self.check_connection(state, boxes.index(1), boxes.index(3))

    # It returns the distance between two given boxes
    def get_distance_boxes(self, box_index_1, box_index_2):
        box_row_1 = int((box_index_1+1)/self.size)
        box_row_2 = int((box_index_2+1)/self.size)
        box_col_1 = int((box_index_1+1) % self.size)
        box_col_2 = int((box_index_2+1) % self.size)
        return abs(box_col_2-box_col_1)+abs(box_row_2-box_row_1)

    # Based on a given
    def reward(self, state, action):
        boxes, _ = state
        total_reward = 0
        L = [1, 2, 3]
        if 'Place' in action:
            total_reward += self.place_point
            if 'start' in action:
                number = 1
            elif 'exit' in action:
                number = 2
            else:
                number = 3
            L.remove(number)
            for i in L:
                if i in boxes:
                    if self.check_connection(state, boxes.index(number), boxes.index(i)):
                        total_reward += self.create_connection
                    total_reward += self.distance_reward * \
                        self.get_distance_boxes(
                            boxes.index(number), boxes.index(i))
        if 'Wall' in action:
            total_reward += self.break_wall

        return total_reward

    def draw_wall(self, wall, i):
        if wall == 0:
            return ' '
        else:
            # Vertical wall
            if i % 2 == 0:
                return '|'
            # Horizontal wall
            else:
                return '-'

    def draw_box(self, box_number):
        if box_number == 1:
            return 'S'
        elif box_number == 2:
            return 'E'
        elif box_number == 3:
            return 'T'
        else:
            return ' '

    def draw(self, state):
        boxes, walls = state
        L = []
        for i in range(0, 2*self.size-1):
            s = ""
            p = ""
            if i % 2 == 0:

                for b in range((i//2)*self.size, ((i//2)+1)*self.size):
                    s = s+self.draw_box(boxes[b])+'='
                s = s[:-1]

                for w in range((i//2)*(2*self.size-1), (i//2)*(2*self.size-1)+self.size-1):
                    p = p+self.draw_wall(walls[w], i)

                for elt in p:
                    s = ''.join(s[:s.find('=')]+elt+s[s.find('=')+1:])

            else:
                for w in range(((i+1)//2)*(self.size-1)+((i-1)//2)*self.size, ((i+1)//2)*(self.size-1)+((i-1)//2)*self.size+self.size):
                    s = s + self.draw_wall(walls[w], i)+' '
                s = s[:-1]
            L.append(s)
        length = max(len(word) for word in L)
        print()
        print(' ' * 4 + '*' * (length + 4))
        for word in L:
            print(' ' * 4 + '* {:<{}} *'.format(word, length))
        print(' ' * 4 + '*' * (length + 4))
        print()