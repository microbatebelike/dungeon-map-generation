import sys
from Environment import Environment
from Agent import Agent

if __name__=='__main__': 
    file_path=sys.argv[1]
    environment = Environment(4)
    agent = Agent(environment)
    agent.train(10000,file_path)
    
