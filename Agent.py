import sys
import numpy as np
from time import sleep  
import json 
from tqdm import tqdm


# This class represents the Ai that controls the game. It allow us : 
# 1 - Choose the next action for any given state  
# 2 - Perform the chosen action and transition to the next state
# 3 - Receive the reward and transition to the next state 
# 4 - Receive a reward and compute the temporal difference
# 5 - Update Q-value for Previous state


class Agent:
    
    def __init__(self, environment, alpha = 0.1, gamma = 0.9, epsilon = 0.1):
        self.environment = environment 
        self.alpha = alpha 
        self.gamma = gamma 
        self.epsilon = epsilon 
        self.qvalues_dictionnary = {}
    
    
    # Returns the maximum q_value for a given state
    def get_max_q(self,state):
        if not self.qvalues_dictionnary.get(state):
            return 0 
        else : 
            return max(self.qvalues_dictionnary.get(state).values())

    # Returns the q_value of an action in a given state
    def get_qvalue(self,state,action):
        if not self.qvalues_dictionnary.get(state):
            return 0 
        else : 
            if not self.qvalues_dictionnary.get(state).get(action):
                return 0
            else :
                return self.qvalues_dictionnary.get(state).get(action)
            
    # Calculate the temporal difference when choosing an action in a given state.
    # It represents how much the q_value for the action taken in the previous state
    # should be changed based on what the AI agent has learned about the q_value for 
    # the current state's action
    def temporal_difference(self,current_state,chosen_action):
        new_state=self.environment.update_state(current_state,chosen_action)
        reward=self.environment.reward(new_state,chosen_action)
        max_next_q = self.get_max_q(str(new_state))
        current_q = self.get_qvalue(str(current_state),chosen_action)
        return reward+(self.gamma)*max_next_q-current_q     
    
    # Update the q_values dictionnary using the Bellman Equation
    def update_qvalues_dictionnary(self,current_state,chosen_action): 
        new_qvalue=self.get_qvalue(str(current_state),chosen_action)+self.alpha*self.temporal_difference(current_state,chosen_action)
        self.qvalues_dictionnary[str(current_state)]={chosen_action:new_qvalue}
        return new_qvalue

    # Select the best possible action in a given state if it is present in the q-value dictionnary
    def best_action_q(self,state):
        if self.qvalues_dictionnary.get(str(state)) :
            key_list = [k for (k, val) in self.qvalues_dictionnary.get(str(state)).items() if val == max(self.qvalues_dictionnary.get(str(state)).values())]
            return key_list[0]
        else :
            return ''
    
    # Select the next action to be executed using the epsilon-greedy algorithm 
    def get_next_action(self,state):
        action = self.best_action_q(state)
        if (np.random.random() < self.epsilon ) and (action != '' ) and ( action in self.environment.get_list_of_actions() ):
            return action
        else: 
            actions_list=self.environment.get_list_of_actions() 
            return actions_list[np.random.randint(len(actions_list))]
    
    # Train the AI agent to generate x mazes satisfying the 
    def train(self, iterations,file_path):
        for i in tqdm(range(iterations)):
            state = self.environment.initialize()
            while not self.environment.is_final_state(state):   
                action = self.get_next_action(state)
                self.update_qvalues_dictionnary(state, action)
                state=self.environment.update_state(state,action)
                self.environment.update_available_actions(action)
            with open(file_path, "w") as write_file:
                json.dump(self.qvalues_dictionnary, write_file, indent=2)
        return state

    def generate(self,qdictionnary_path=''):
        if qdictionnary_path != '':
            try:
                with open(qdictionnary_path) as json_file:
                    self.qvalues_dictionnary = json.load(json_file)
            except:
                print('Error while loading Q values dictionnary')
        state = self.environment.initialize()
        while not self.environment.is_final_state(state):   
            action = self.get_next_action(state)
            state=self.environment.update_state(state,action)
            self.environment.update_available_actions(action)
        return state
    
    