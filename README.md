# Generative Dungeon Maps

Dans le cadre du test technique de l'entreprise IMKI, il m'a été proposé de mettre en place un agent intelligent permettant de générer des cartes de Dungeon. 
Les cartes de Dungeon ont la particularité d'avoir trois points principaux : le point de départ, le point d'arrivée et le point du trésor. Pour qu'une carte de Dungeon soit valide il faut qu'il existe un chemin menant du point de départ au point d'arrivée et du point de départ au trésor.

L'agent intelligent utilisé a été entraîné en utilisant du q-learning qui est une méthode d'apprentissage par renforcement.

## Conception 
### L'état
Tout d'abord il fallait représenter la carte. Le choix était d'utiliser un couple de listes où la première représente l'État des cases et la deuxième représente l'État des murs. 
Ces deux listes sont indexées comme indiquées dans la photo suivante;
Les indices en rouge sont ceux utilisés pour les cases et en vert sont ceux utilisés pour les murs. 

On a décidé de partir d'un état initiale où la carte ne contient aucun point et où tous les murs sont bloquants.

### Les actions
L'ensemble des actions possibles sont divisées en quatre sous-groupes : 
* Placer le point de départ dans une case x
* Placer le point d'arrivée dans une case x 
* Placer le trésor dans une case x 
* Détruire le mur y 

Avec x et y représentant successivement le numéro de la case et le numéro du mur.

### Les récompenses
Pour la récompense on a décidé d'offrir 4 types de récompenses dépendant de l'action choisie :  
* Placement d'un nouveau point : l'agent s'attribuera une récompense de 3 points à chaque fois qu'il y a un nouveau placement, cela va lui permettre de comprendre l'importance de cette action et ainsi la favoriser. 
* Destruction d'un mur : l'agent s'attribuera un malus d'un point pour chaque mur détruit, cela lui permettra de détruire le minimum de murs possible (éviter aussi la solution évidente où on détruit tous les murs).
* Favorisation des placements distants : afin que nos trois points soient positionnés d'une manière non évidente, l'agent s'attribuera un bonus d'un point pour chaque case qui va séparer le nouveau point par rapport aux existant lors d'un nouveau positionnement. 
* Création d'une connexion : Enfin pour chaque nouvelle connexion créée l'agent va s'attribuer un bonus de 10 points.

### La Q-table 
Elle représente la table qui permet de déterminer la valeur de la qualité du choix d'une action dans un état donné. Dans notre cas, vu qu'on a un nombre très important d'états qui ne sont pas faciles à énumérer, on a décidé d'utiliser un dictionnaire de dictionnaire. Dans le premier niveau les états représentant les clés et les valeurs seront des dictionnaires sauvegardant pour chaque action donnée (clé) une valeur de qualité (q-value) correspondante.

## Architecture
Pour l'implémentation de la solution deux classes ont été implémenté.
### Environment 
Une classe qui représente l'environnement globale du jeu. Il nous procure toutes les informations à propos du jeu. 

### Agent 
Une classe qui représente l'agent controlant le jeu. Il est à l'origine de la gestion des choix d'actions et de la modification des valeurs de qualités. 

## Utilisation

Afin de lancer l'entraînement, il faut lancer la commande suivante en spécifiant le chemin vers le fichier qui va contenir la Q-table.
```bash
python train.py file_name
```
Afin de génerer une carte de Dungeon, il faut lancer la commande suivante en spécifiant le nom du fichier depuis lequel on veut récuperer la Q-table. Le fichier q_dict.json propose une q-table d'un entraînement pré-effectué qui pourra être utiliser.
```bash
python generate.py file_name
```

## Réalisation
Dans la figure suivante, on peut voir deux maps de dungeon générés par l'agent intelligent qui sont successivement de taille 4x4 et 6x6. S représente le point de départ (Starting point), E représente le point de sortie (Exit Point), T représente le trésor (Tresor), '|' représene un mur verticale et '_' représente un mur horizontale. 

![4x4 Map](Example 1.png)

![6x6 Map](Example2.png)

# Suite du travail :
## Etudes des hyperparamètres : 
Une étude pour déterminer les valuers optimales des différents hyperparamètre (lerning rate, gamma, epsilon) ne peut qu'améliorer les résultats obtenues.
 
## Métrique de déterminaison de difficulté 
Comme métrique permettant de définir la difficulté de la résolution d'une carte, on peut retrouver le nombre de chemins distincts existants, vu que s'il y a plusieurs chemins existant entre l'entrée et la sortie cela veut dire que la carte est facile à résoudre. 
Autre point indicateur pourrait être le nombre de murs qui ont été détruits, plus on a des murs détruits plus la carte est facile à résoudre.