import sys
from Environment import Environment
from Agent import Agent

if __name__=='__main__': 
    file_path=sys.argv[1]
    environment = Environment(6)
    agent = Agent(environment)
    (agent.environment.draw(agent.generate(file_path)))